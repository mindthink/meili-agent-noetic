import rospy


def log_info(message):
    rospy.loginfo(message)


def log_error(message):
    rospy.logerr(message)
