#!/usr/bin/env python3

import rospy
from logging_ros import log_info, log_error

import actionlib
import meili_agent.msg
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Bool

from meili_ros_lib.docking import DockingActionServer


class DockingActionServerROS(DockingActionServer):
    # create messages that are used to publish feedback/result
    _feedback = meili_agent.msg.DockingFeedback()
    _result = meili_agent.msg.DockingResult()

    def __init__(self, name):
        DockingActionServer.__init__(self, name)

        self.log_info = log_info
        self.log_error = log_error

        self._as = actionlib.SimpleActionServer(self.action_name, meili_agent.msg.DockingAction,
                                                execute_cb=self.execute_cb, auto_start=False)
        self._as.start()
        self.log_info("[DockingServer] Docking Server Initialized")

    def execute_cb(self, goal):
        self.plan = Path()
        self.recording = True
        self.log_info("[DockingServer] Executing recording callback")
        self.subscribers(goal)

        success = False
        while not success:
            # self.log_info(f"[NOT success] {self.recording == False}")
            if self._as.is_preempt_requested():
                self.log_info(f"[DockingServer] {self.action_name} preempted")
                self._as.set_preempted()
                break
            if not self.recording:
                success = True

        if success:
            self._result.plan = self.plan
            self.log_info(f"[DockingServer] {self.action_name} succeeded")
            self._as.set_succeeded(self._result)

    def subscribers(self, goal):
        vehicle_prefix = goal.vehicle_prefix.data
        rospy.Subscriber(
            vehicle_prefix + "/amcl_pose",
            PoseWithCovarianceStamped,
            self.callback_pose,
            tcp_nodelay=True,
        )

        rospy.Subscriber(
            vehicle_prefix + "/recording",
            Bool,
            self.callback_recording,
            tcp_nodelay=True,
        )


if __name__ == "__main__":
    rospy.init_node("docking")
    server = DockingActionServerROS(rospy.get_name())
    rospy.spin()
