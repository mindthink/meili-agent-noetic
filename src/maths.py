"""Math helper"""
import math

from sentry_sdk import capture_exception


def quaternion_to_euler(x_c, y_c, z_c, w_c):
    """Transform quaternion to euler"""
    try:
        # conversion between ros quaternion into euler angles
        # t0 = +2.0 * (w * x + y * z)
        # t1 = +1.0 - 2.0 * (x * x + y * y)
        # roll = math.degrees(math.atan2(t0, t1))

        # t2 = +2.0 * (w * y - z * x)
        # t2 = +1.0 if t2 > +1.0 else t2
        # t2 = -1.0 if t2 < -1.0 else t2
        # pitch = math.degrees(math.asin(t2))

        t_3 = +2.0 * (w_c * z_c + x_c * y_c)
        t_4 = +1.0 - 2.0 * (y_c * y_c + z_c * z_c)
        yaw = math.degrees(math.atan2(t_3, t_4))

    except TypeError:
        capture_exception(TypeError)
        raise

    return yaw


def euler_to_quaternion(roll, pitch, yaw):
    """Transform euler to quaternion"""
    try:
        # Conversion Between euler angles to quaternion
        q_x = math.sin(roll / 2) * math.cos(pitch / 2) * math.cos(yaw / 2) - math.cos(
            roll / 2
        ) * math.sin(pitch / 2) * math.sin(yaw / 2)
        q_y = math.cos(roll / 2) * math.sin(pitch / 2) * math.cos(yaw / 2) + math.sin(
            roll / 2
        ) * math.cos(pitch / 2) * math.sin(yaw / 2)
        q_z = math.cos(roll / 2) * math.cos(pitch / 2) * math.sin(yaw / 2) - math.sin(
            roll / 2
        ) * math.sin(pitch / 2) * math.cos(yaw / 2)
        q_w = math.cos(roll / 2) * math.cos(pitch / 2) * math.cos(yaw / 2) + math.sin(
            roll / 2
        ) * math.sin(pitch / 2) * math.sin(yaw / 2)

        rotation_quaternion = {"qx": q_x, "qy": q_y, "qw": q_w, "qz": q_z}

    except TypeError:
        capture_exception(TypeError)
        raise

    return rotation_quaternion
