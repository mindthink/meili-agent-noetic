#!/usr/bin/env python3

"""Principal Agent. Initialization of node"""
import logging
# standard python3 libraries
import signal
import sys
# ROS1 dependencies
import rospy

# Meili agent dependencies
from logging_ros import log_info, log_error

from agent import MeiliAgent
from handlers import SDKHandlers
from publishers_subscribers import pub_and_sub
from sentry_sdk import capture_exception
from parameter_store import ParameterStore

from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.agent_setup import Setup
from meili_ros_lib.connection import Connection

initialize_sentry()


class InitiationNode(Setup):
    """Class for creating ROS node."""

    def __init__(self):
        # Node initialization
        rospy.init_node("agent")

        # General Setup
        Setup.__init__(self)

        # ROS1
        # Logging
        self.log_info = log_info
        self.log_error = log_error

        self.init_parameters_setup()
        self.get_launch_parameters()

        self.parameter_store = ParameterStore(
            self.aws_access_key_id, self.aws_secret_access_key
        )

    def get_launch_parameters(self):
        try:
            # ROS1
            self.path_planning = rospy.get_param("path_planning")
            self.traffic_control = rospy.get_param("traffic_control")
            battery = rospy.get_param("battery_present")
            self.publish_frequency = rospy.get_param("publish_frequency")
            self.topic_streaming_mode = rospy.get_param("topic_streaming_mode")
            self.offline_flag = rospy.get_param("offline")

            self.battery_parameter(battery)

        except KeyError:
            self.log_error("[ROSAgent] Parameter does not exist. ")
            capture_exception(KeyError)
            raise


def main():
    # mqtt logging active
    logging.basicConfig(level=logging.DEBUG)
    try:
        ################################################
        # SETUP
        node = InitiationNode()
        agent = MeiliAgent(node)
        handlers = SDKHandlers(agent)
        agent.connection = Connection(
            node, handlers,
            agent.vehicle_list,
        )

        ################################################
        # ROS1 SUBSCRIBERS AND PUBLISHERS
        pub_and_sub(node, agent)

        node.log_info("[ROSAgentNode] Meili Agent is ready for receiving tasks")

        rate = rospy.Rate(1)
        pub_rate = rospy.Rate(node.publish_frequency)
        agent.run(rate, pub_rate)
        rospy.spin()
    except Exception as e:
        log_info(f"[ROSAgent] Error in the main run: {e}")


def exit_gracefully(_, __):
    """Exiting gracefully the program"""
    sys.exit(0)


if __name__ == "__main__":
    try:
        signal.signal(signal.SIGINT, exit_gracefully)
        main()
    except Exception as e:
        capture_exception(e)
