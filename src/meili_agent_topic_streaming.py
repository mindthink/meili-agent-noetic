#!/usr/bin/env python3
import logging
import signal
import sys
import time
from importlib import import_module
from os.path import abspath, dirname, expanduser, join

import rospy
import yaml

# influx dependencies
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import SYNCHRONOUS
from meili_agent.msg import Topicinit

# ROS dependencies
from rospy_message_converter import message_converter
from sentry_sdk import capture_exception

# MQTT dependencies
from mqtt_client import MQTTClient


class Topic_msg_server:
    """
    Class to subscribe, store and send ROS topics to Meili Cloud using InfluxDB connector or MQTT
    """

    def __init__(self):
        self.msg = []
        self.last_msg = []
        self.topic_uuid = []
        self.message_type = []
        self.vehicle_token = []
        self.topic = []
        self.mqtt = None
        self.block = False

        self._binary_sub = None
        self.now = None
        self.mqtt_id = None
        self.publish_frequency = None
        self.topic_streaming_mode = None
        self.topic_streaming_method = "influx"

        self.influx_bucket = None
        self.influx_org = None
        self.influx_token = None
        self.influx_url = None
        self.influx_write_api = None
        self.influx_client = None
        self.influx_data = None

        self.total_count = 0
        self.number_of_vehicles = 0
        self.vehicle_order = []
        self.prefix = []

    def binary_callback(self, data, topic_info):

        rate = rospy.Rate(1)
        # binary callback for subscribe necessary topics to be streamed
        assert sys.version_info >= (2, 7)  # import_module's syntax needs 2.7
        connection_header = data._connection_header["type"].split("/")
        ros_pkg = connection_header[0] + ".msg"
        self.msg_type = connection_header[1]
        topic_info["msg_type"] = self.msg_type
        msg_class = getattr(import_module(ros_pkg), self.msg_type)
        self._binary_sub.unregister()
        self._deserialized_sub = rospy.Subscriber(
            topic_info["topic"],
            msg_class,
            self.deserialized_callback,
            topic_info,
            tcp_nodelay=True,
        )

    def deserialized_callback(self, data, topic_info):
        # Converting ros msg into dictionary for passing to the FMS server
        rate = rospy.Rate(1.0)
        msg_converted = message_converter.convert_ros_message_to_dictionary(data)
        # REAL TIME
        if self.topic_streaming_mode == "REALTIME":
            if self.topic_streaming_method == "mqtt":
                self.mqtt.send_topic_data(
                    topic_info["topic_uuid"],
                    msg_converted,
                    topic_info["vehicle_token"],
                    topic_info["msg_type"],
                    topic_info["topic"],
                )
            # Influx method
            else:
                data = [
                    {
                        "measurement": "topic",
                        "tags": {
                            "topic": topic_info["topic"],
                            "vehicle": topic_info["vehicle_token"],
                        },
                        "fields": {
                            "topic_uuid": topic_info["topic_uuid"],
                            "msg": str(msg_converted),
                        },
                    }
                ]
                self.influx_write_api.write(self.influx_bucket, self.influx_org, data)
        # FILTERING
        else:
            for index in range(0, self.number_of_vehicles):
                for index2 in range(0, self.total_count):
                    self.msg[topic_info["vehicle_order"] - 1][
                        topic_info["topic_order"] - 1
                    ] = msg_converted

    def callback(self, data):
        # this callback is for getting the topic info coming from ROSAgent node

        msg = data
        rate = rospy.Rate(1.0)
        self.vehicle_token.append(msg.vehicle_token)
        self.topic_uuid.append(msg.topic_uuid)
        self.topic.append(msg.topic_name)
        self.message_type.append(msg.topic_type)
        self.prefix.append(msg.prefix)

        topic_info = {}
        topic_info["topic"] = msg.topic_name
        topic_info["topic_uuid"] = msg.topic_uuid
        topic_info["vehicle_token"] = msg.vehicle_token
        topic_info["topic_order"] = int(msg.total_count)
        topic_info["vehicle_order"] = int(msg.number_of_vehicles)

        self.total_count = int(msg.total_count)
        self.number_of_vehicles = topic_info["vehicle_order"]
        w, h = self.total_count, self.number_of_vehicles
        self.msg = [[0 for x in range(w)] for y in range(h)]
        self.last_msg = [[0 for x in range(w)] for y in range(h)]
        self._binary_sub = rospy.Subscriber(
            msg.topic_name, rospy.AnyMsg, self.binary_callback, topic_info
        )
        rate.sleep()

    def run(self):
        # if mode selected is FILTERING, msg will be sent to the server to the predefined freauency
        rate = rospy.Rate(self.publish_frequency)
        time.sleep(2)
        if self.topic_streaming_mode == "FILTERING":
            self.log_info(
                "[ROSAgent_TOPIC_STREAMING] Mode for topic streaming is FILTERING"
            )
            while True:
                if exit == 1:
                    self.mqtt.disconnect()
                    sys.exit(0)
                rate.sleep()
                for index in range(0, self.number_of_vehicles):
                    if self.topic_streaming_method == "mqtt":
                        self.mqtt.send_topic_data(
                            self.topic_uuid[index],
                            self.msg[index],
                            self.vehicle_token[index],
                            self.message_type[index],
                            self.topic[index],
                        )
                    elif self.topic_streaming_method == "influx":
                        for index2 in range(0, self.total_count):

                            if self.prefix[index2] != 0:
                                topic_odom = self.prefix[index2] + "/odom"
                                topic_imu = self.prefix[index2] + "/imu"
                                topic_amcl = self.prefix[index2] + "/amcl_pose"
                                topic_vel = self.prefix[index2] + "/cmd_vel"
                                topic_logs = "/rosout"

                            if self.topic[index2] == topic_odom:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[index],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "xpos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["x"],
                                                "ypos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["y"],
                                                "zpos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["z"],
                                            },
                                        }
                                    ]
                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )

                            if self.topic[index2] == topic_amcl:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[index],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "xpos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["x"],
                                                "ypos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["y"],
                                                "zpos": self.msg[index][index2]["pose"][
                                                    "pose"
                                                ]["position"]["z"],
                                            },
                                        }
                                    ]
                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )

                            elif self.topic[index2] == topic_imu:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[index],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "x.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["x"],
                                                "y.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["y"],
                                                "z.orientation": self.msg[index][
                                                    index2
                                                ]["orientation"]["z"],
                                                "x.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["x"],
                                                "y.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["y"],
                                                "z.angular_vel": self.msg[index][
                                                    index2
                                                ]["angular_velocity"]["z"],
                                                "x.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["x"],
                                                "y.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["y"],
                                                "z.linear_acce": self.msg[index][
                                                    index2
                                                ]["linear_acceleration"]["z"],
                                            },
                                        }
                                    ]
                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )
                            elif self.topic[index2] == topic_vel:
                                if self.msg[index][index2] != 0:
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[index],
                                            },
                                            "fields": {
                                                "topic_uuid": str(self.topic_uuid),
                                                "msg": str(self.msg[index][index2]),
                                                "x.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["x"],
                                                "y.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["y"],
                                                "z.linear": self.msg[index][index2][
                                                    "linear"
                                                ]["z"],
                                                "x.angular": self.msg[index][index2][
                                                    "angular"
                                                ]["x"],
                                                "y.angular": self.msg[index][index2][
                                                    "angular"
                                                ]["y"],
                                                "z.angular": self.msg[index][index2][
                                                    "angular"
                                                ]["z"],
                                            },
                                        }
                                    ]

                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )

                            elif self.topic[index2] == topic_logs:
                                if (
                                    self.msg[index][index2] != 0
                                    and self.msg[index][index2]
                                    != self.last_msg[index][index2]
                                ):
                                    self.last_msg[index][index2] = self.msg[index][
                                        index2
                                    ]
                                    data = [
                                        {
                                            "measurement": "topic",
                                            "tags": {
                                                "topic": self.topic[index2],
                                                "vehicle": self.vehicle_token[index],
                                            },
                                            "fields": {
                                                "topic_uuid": str(
                                                    self.topic_uuid[index2]
                                                ),
                                                "msg": str(self.msg[index][index2]),
                                                "level": self.msg[index][index2][
                                                    "level"
                                                ],
                                            },
                                        }
                                    ]
                                    self.influx_write_api.write(
                                        self.influx_bucket, self.influx_org, data
                                    )
                            else:
                                data = [
                                    {
                                        "measurement": "topic",
                                        "tags": {
                                            "topic": self.topic[index2],
                                            "vehicle": self.vehicle_token[index],
                                        },
                                        "fields": {
                                            "topic_uuid": str(self.topic_uuid[index2]),
                                            "msg": str(self.msg[index][index2]),
                                        },
                                    }
                                ]
                                self.influx_write_api.write(
                                    self.influx_bucket, self.influx_org, data
                                )

        else:
            self.log_info(
                "[ROSAgent_TOPIC_STREAMING] Mode for topic streaming is REALTIME"
            )

    def config_agent(self):
        if len(sys.argv) < 1:
            self.log_info("usage: agent.py pathToConfigTopics")
        else:
            self.influx_bucket = sys.argv[3]
            self.influx_org = sys.argv[4]

        # config agent by reading and storing config files about number of vehicles, prefixes and topics
        homedir = expanduser("~")
        config_file_path = ""
        cfg_file_path = join(homedir, ".meili", "cfg.yaml")
        dir_config = dirname(dirname(abspath(__file__)))

        if len(sys.argv) < 1:
            self.log_info("[ROSAgent]usage: agent.py pathToConfigTopics")
        else:
            config_vehicles_path = sys.argv[2]

        filenamevehicles = abspath(join(dir_config, config_vehicles_path))
        data = None
        # Parse Mode, number of vehicles and tokens from config files passed as arguments
        with open(filenamevehicles, "r") as config:
            data = yaml.safe_load(config)

        if data is None:
            sys.exit()

        try:
            self.influx_data = data["influx_details"]
            self.influx_token = self.influx_data["token"]
            self.influx_url = self.influx_data["host"]
            if (self.influx_url or self.influx_token) == None:

                log_msg = (
                    "[ROSAgent_TOPIC_STREAMING] Influx credentials are not available and meili_agent_streamer node will be shut down."
                    + "\n"
                    + "If you need topic streaming capability, please request it from Meili team."
                    + "\n"
                    + "Meili Agent continues to work normally! You can start sending tasks to the robot."
                )
                sys.exit()

        except Exception as e:
            capture_exception(e)
            self.log_info(f"[ROSAgent]1 Excepcion: {e} ")


def Exit_gracefully(signal, frame):
    sys.exit(0)


def main(args=None):
    logging.basicConfig(level=logging.DEBUG)
    topic_msg_server = Topic_msg_server()
    rospy.init_node("topic_streaming")
    r = rospy.Rate(1)
    rospy.Subscriber("/topic_init", Topicinit, topic_msg_server.callback)
    self.log_info("[ROSAgent_TOPIC_STREAMING] Topic init is subscribe")

    topic_msg_server.config_agent()

    r.sleep()

    try:
        topic_msg_server.topic_streaming_mode = rospy.get_param("/topic_streaming_mode")
    except:
        capture_exception(e)
        topic_msg_server.topic_streaming_mode = "FILTERING"
        rospy.logerr(
            "[ROSAgent_TOPIC_STREAMING] /streaming_mode parameter is not set, FILTERING mode is the default mode"
        )

    try:
        topic_msg_server.publish_frequency = rospy.get_param("/publish_frequency")
        self.log_info(
            "[ROSAgent_TOPIC_STREAMING] Publish frequency for topic streaming is: %f",
            topic_msg_server.publish_frequency,
        )
    except:
        capture_exception(e)
        rospy.logerr(
            "[ROSAgent_TOPIC_STREAMING] publish frequency is not set up, using 1hz"
        )
        topic_msg_server.publish_frequency = 1
    try:
        if topic_msg_server.topic_streaming_method == "mqtt":
            try:
                if rospy.has_param("/mqtt_id"):
                    topic_msg_server.mqtt_id = rospy.get_param("/mqtt_id")
            except:
                capture_exception(e)
                rospy.logerr(
                    "[ROSAgent_TOPIC_STREAMING] /mqtt_id parameter is not set, check your config file, closing streaming topics node"
                )
                sys.exit(1)
            self.log_info(
                "[ROSAgent_TOPIC_STREAMING]mqtt id : %s", topic_msg_server.mqtt_id
            )
            client_id = topic_msg_server.mqtt_id
            topic_msg_server.mqtt = MQTTClient(client_id=client_id)
            topic_msg_server.mqtt.start()
        elif topic_msg_server.topic_streaming_method == "influx":
            topic_msg_server.influx_client = InfluxDBClient(
                url=topic_msg_server.influx_url, token=topic_msg_server.influx_token
            )
            topic_msg_server.influx_write_api = (
                topic_msg_server.influx_client.write_api(write_options=SYNCHRONOUS)
            )

        topic_msg_server.run()
        rospy.spin()
    except Exception as e:
        self.log_info("2 Exception is %s", e)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, Exit_gracefully)
    main()
