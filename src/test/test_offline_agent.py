#!/usr/bin/env python3
# standart python3 libraries
import datetime
import sys
from unittest import TestCase, mock

import rostest
from freezegun import freeze_time

from offline_agent import OfflineAgent


class MockClient:
    def __init__(self, o):
        self.o = o

    def run_in_thread(self):
        pass

    def add_vehicle(self, index):
        pass

    class Connection:
        def __init__(self):
            pass

        def close(self):
            self.o.ws_open = False
            return "ws_close"


class ConfigAgentTestCase(TestCase):
    def setUp(self):
        def sdk_setup():
            client = MockClient(self.o)
            return client

        self.o = OfflineAgent(
            sdk_setup,
            "number_of_vehicles",
            "task_handler",
            "waypoints_handler",
            ["123", "455"],
        )

    def test_internet_connected(self):

        ic = self.o.internet_connected()
        self.assertEqual(ic, 1)

        ic = self.o.internet_connected()
        self.assertEqual(ic, 3)

        self.assertFalse(self.o.offline)

    def test_config_weekdays_wrongdata(self):
        task = {"week_days": ["a", "b", 2, 3, 4, 5, 6]}
        with self.assertRaises(TypeError):  # , "Wrong data of Week Days"):
            config_weekdays(task)

    def test_config_weekdays_wrongday(self):
        task = {"week_days": [1, 8, 2, 3, 4, 5, 6]}
        with self.assertRaises(TypeError):  # , "Wrong data of Week Days"):
            config_weekdays(task)

    def test_config_weekdays_output(self):
        task = {"week_days": {0, 1, 4, 6}}
        weeks_days = config_weekdays(task)
        self.assertEqual(weeks_days, [0, 1, 4, 6])

        task = {"week_days": {}}
        weeks_days = config_weekdays(task)
        self.assertEqual(weeks_days, [0, 1, 2, 3, 4, 5, 6])

    def test_config_goal_wrong_data(self):

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": 123,
                            "x": -2,
                            "y": 1,
                            "rotation": 0,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(TypeError):
            config_goal(task)

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": "abc",
                            "y": 1,
                            "rotation": 0,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(TypeError):
            config_goal(task)

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": 1,
                            "y": "def",
                            "rotation": 0,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(TypeError):
            config_goal(task)

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": 1,
                            "y": 2,
                            "rotation": -90,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(TypeError):
            config_goal(task)

    def test_config_goal_empty_data(self):

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": "",
                            "y": 1,
                            "rotation": 0,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(AssertionError):
            config_goal(task)

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": -2,
                            "y": "",
                            "rotation": 0,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(AssertionError):
            config_goal(task)

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "571a4a9863e54fd1b5f84b7b1c135df6",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": -2,
                            "y": 1,
                            "rotation": "",
                        },
                    }
                ]
            }
        }

        with self.assertRaises(AssertionError):
            config_goal(task)

    def test_config_loc_uuid_empty_data(self):

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": "",
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": -2,
                            "y": 1,
                            "rotation": "",
                        },
                    }
                ]
            }
        }

        with self.assertRaises(AssertionError):
            config_goal(task)

    def test_config_loc_uuid_wrong_data(self):

        task = {
            "task_preset": {
                "subtasks": [
                    {
                        "index": 1,
                        "indoor_point": {
                            "uuid": 123687,
                            "name": "Station 5",
                            "point_type": "drop_off",
                            "x": 1,
                            "y": "1",
                            "rotation": -90,
                        },
                    }
                ]
            }
        }

        with self.assertRaises(TypeError):
            config_goal(task)

    def test_config_time_wrong_data(self):
        task = {"time": "12:433", "cron": ""}
        with self.assertRaises(AssertionError):
            config_time(task)

        task = {"time": "12-43", "cron": ""}
        with self.assertRaises(BaseException):
            config_time(task)

    def test_config_time(self):

        task = {"time": "12:43", "cron": ""}
        time = config_time(task)
        self.assertEqual(time, ["12:43"])

        task = {"time": "12:43", "cron": ""}
        time = config_time(task)
        self.assertEqual(time, ["12:43"])

        task = {"time": "22:00", "cron": "*/60 * * * *"}
        time = config_time(task)
        self.assertEqual(time, ["22:00", "23:00", "24:00"])

    @mock.patch("offline_agent.OfflineAgent.open_yaml", return_value=None)
    def test_config_offlinetasks_empty_data(self, mock_openyaml):
        self.o.offline = True
        with self.assertRaises(AssertionError):
            self.o.config_offlinetasks()

    @mock.patch(
        "offline_agent.OfflineAgent.open_yaml",
        return_value=[{"uuod": 123, "vehicle": {"uuid": "456"}}],
    )
    def test_config_offlinetasks_wrong_data(self, mock_openyaml):
        self.o.offline = True
        self.o.number_of_vehicles = 1
        with self.assertRaises(TypeError):
            self.o.config_offlinetasks()

    @mock.patch(
        "offline_agent.OfflineAgent.open_yaml",
        return_value=[{"uuod": "", "vehicle": {"uuid": "456"}}],
    )
    def test_config_offlinetasks_empty_data(self, mock_openyaml):
        self.o.offline = True
        self.o.number_of_vehicles = 1
        with self.assertRaises(AssertionError):
            self.o.config_offlinetasks()

    @mock.patch(
        "offline_agent.OfflineAgent.open_yaml",
        return_value=[
            {
                "uuod": "123",
                "name": "abc",
                "time": "12:42",
                "week_days": [0, 1, 5, 6],
                "cron": "",
                "vehicle": {"uuid": "456", "verbose_name": "fearless coati"},
                "task_preset": {
                    "subtasks": [
                        {
                            "index": 1,
                            "indoor_point": {
                                "uuid": "789",
                                "name": "Station 5",
                                "point_type": "drop_off",
                                "x": -2,
                                "y": 1,
                                "rotation": 1,
                            },
                        }
                    ]
                },
            }
        ],
    )
    def test_config_offlinetasks(self, mock_openyaml):

        self.o.offline = True
        self.o.number_of_vehicles = 1
        offline_tasks = self.o.config_offlinetasks()
        self.assertEqual(
            offline_tasks,
            [
                [
                    "123",
                    [0, 1, 5, 6],
                    ["12:42"],
                    "456",
                    [["drop_off", -2, 1, 1]],
                    ["789"],
                ]
            ],
        )

    def test_get_vehicle_position(self):
        vehicle_list = ["1", "2", "4"]
        vehicle_uuid = "456"

        with self.assertRaises(ValueError):
            position = get_vehicle_position(vehicle_list, vehicle_uuid)
            self.assertEqual(position, 0)

        vehicle_list = ["1", "456", "4"]
        vehicle_uuid = "456"
        position = get_vehicle_position(vehicle_list, vehicle_uuid)

        self.assertEqual(position, 1)

    @freeze_time("2020-11-29 12:42:20.850594")
    @mock.patch("offline_agent.OfflineAgent.offline_task_handler")
    def test_offlinetaskexecuter_tasknotstarted(self, mock_offline_task_handler):
        today = datetime.date.today().weekday()
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M")

        offline_tasks = [
            ["123", [0, 1, 5, 6], ["12:42"], "456", [["drop_off", -2, 1, 1]], ["789"]]
        ]
        vehicle_list = ["12a", "456"]
        task_offline_started = False
        self.o.last_executed_time = [0, 0]

        current_task_uuid, task_offline_started = self.o.offlinetaskexecuter(
            offline_tasks, vehicle_list, task_offline_started
        )

        self.assertTrue(mock_offline_task_handler.called)
        self.assertEqual(current_task_uuid, "123")
        self.assertTrue(task_offline_started)

    @mock.patch("offline_agent.OfflineAgent.offline_task_handler")
    @freeze_time("2021-11-29 12:42:20.850594")
    def test_offlinetaskexecuter_taskstarted(self, mock_offline_task_handler):
        offline_tasks = [
            ["123", [0, 1, 5, 6], ["12:42"], "456", [["drop_off", -2, 1, 1]], ["789"]]
        ]
        vehicle_list = ["12a", "456"]
        task_offline_started = True

        self.o.last_executed_time = [0, 0]
        current_task_uuid, task_offline_started = self.o.offlinetaskexecuter(
            offline_tasks, vehicle_list, task_offline_started
        )

        self.assertFalse(mock_offline_task_handler.called)
        self.assertEqual(current_task_uuid, "123")
        self.assertTrue(task_offline_started)

    @mock.patch("offline_agent.OfflineAgent.offline_task_handler")
    @freeze_time("2021-11-29 09:42:20.850594")
    def test_offlinetaskexecuter_wrongtime(self, mock_offline_task_handler):
        offline_tasks = [
            ["123", [0, 1, 5, 6], ["12:42"], "456", [["drop_off", -2, 1, 1]], ["789"]]
        ]
        vehicle_list = ["12a", "456"]
        task_offline_started = False

        self.o.last_executed_time = [0, 0]
        current_task_uuid, task_offline_started = self.o.offlinetaskexecuter(
            offline_tasks, vehicle_list, task_offline_started
        )

        self.assertFalse(mock_offline_task_handler.called)
        self.assertEqual(current_task_uuid, "123")

    @mock.patch("offline_agent.OfflineAgent.offline_task_handler")
    @freeze_time("2021-12-01 12:42:20.850594")
    def test_offlinetaskexecuter_wrongday(self, mock_offline_task_handler):
        offline_tasks = [
            ["123", [0, 1, 5, 6], ["12:42"], "456", [["drop_off", -2, 1, 1]], ["789"]]
        ]
        vehicle_list = ["12a", "456"]
        task_offline_started = False

        self.o.last_executed_time = [0, 0]
        current_task_uuid, task_offline_started = self.o.offlinetaskexecuter(
            offline_tasks, vehicle_list, task_offline_started
        )

        self.assertFalse(mock_offline_task_handler.called)
        self.assertEqual(current_task_uuid, None)


if __name__ == "__main__":
    rostest.rosrun("meili_agent", "test_offline_agent", ConfigAgentTestCase, sys.argv)
