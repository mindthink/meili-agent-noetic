""" Handlers for the incoming info from Meili SDK"""
# !/usr/bin/env python3


# ROS1 dependencies
import threading
import actionlib
import rospy
from std_msgs.msg import Bool, String

# Meili agent dependencies
from meili_agent.msg import DockingGoal
from parse_data import parse_goal, parse_cancel_goal, parse_topic
import meili_agent.msg

# Meili Lib dependencies
from meili_ros_lib.sdk_handlers import Handlers
from meili_ros_lib.sentry import initialize_sentry
from meili_ros_lib.parse_data import goal_setup, rotation_setup, parse_vel

initialize_sentry()


class SDKHandlers(Handlers):
    """Class for all the handlers of messages coming from SDK"""

    def __init__(self, agent):
        Handlers.__init__(self, agent)
        self.rate = rospy.Rate(1)

        # ROS1
        self.slow_down = []
        for i in range(0, self.agent.number_of_vehicles):
            self.slow_down.append(False)

    def send_goal(self, data, vehicle_position):
        """Sends Goal information to specific vehicle"""
        try:
            rate = rospy.Rate(1)
            x_meters, y_meters, rotation_quaternion = goal_setup(data)
            goal = parse_goal(x_meters, y_meters, rotation_quaternion)
            if self.agent.task_started[vehicle_position] == 0:
                # ROS1
                self.agent.goal_publisher[vehicle_position].publish(goal)
                rate.sleep()
        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Send Goal Error >> {e}")

    def send_waypoints(self, pose, vehicle_position):
        x_meters = [item[0] for item in pose]

        # ROS1
        y_meters = [item[1] for item in pose]
        try:
            rotation = [item[2] for item in pose]
        except IndexError:
            rotation = [0 for _ in pose]

        x = threading.Thread(target=self.waypoints_thread,
                             args=(x_meters, y_meters, rotation, vehicle_position,))
        x.start()

        self.agent.log_info(
            f"[ROSHandler] Received waypoints task assigned to vehicle: {self.agent.vehicle_names[vehicle_position]}"
        )

    # ROS1
    def waypoints_thread(self, x_meters, y_meters, rotation, vehicle_position):
        rate = rospy.Rate(0.5)
        number_of_waypoints = len(x_meters)
        count = 0
        while count < number_of_waypoints:
            if self.agent.task_started[vehicle_position] == 0:
                self.agent.log_info(
                    f"[ROSHandler] Number of received waypoints is: {count + 1}/{number_of_waypoints} for "
                    f"{self.agent.vehicle_names[vehicle_position]} "
                )

                if self.agent.pause_paths[vehicle_position]:  # Wait until first waypoint to send FMS the path
                    if count > 0:
                        self.agent.pause_paths[vehicle_position] = False

                rotation_quaternion = rotation_setup(rotation[count])
                goal = parse_goal(x_meters[count], y_meters[count], rotation_quaternion)
                self.agent.goal_publisher[vehicle_position].publish(goal)
                self.agent.number_of_tasks += count
                count += 1

            if not self.agent.waypoints[vehicle_position]:
                break

        self.agent.waypoints[vehicle_position] = False

    def cancel_goal(self, goal_id, vehicle_position):
        # ROS1
        rate = rospy.Rate(1)
        goal_cancel = parse_cancel_goal(goal_id)
        self.agent.goal_canceller[vehicle_position].publish(goal_cancel)
        # self.agent.log_info("[ROSHandler] Waiting to cancel")
        while self.agent.task_started[vehicle_position] == 1:
            rate.sleep()
        self.agent.log_info("[ROSHandler] Goal Canceled")

    def cancel_goal_waypoints(self, goal_id, vehicle_position):
        # ROS1
        rate = rospy.Rate(1)
        goal_cancel = parse_cancel_goal(goal_id)
        self.agent.goal_canceller[vehicle_position].publish(goal_cancel)
        # self.agent.log_info("[ROSHandler] Waiting Waypoints to cancel")
        while self.agent.task_started[vehicle_position] == 1:
            rate.sleep()
        self.agent.log_info("[ROSHandler] Goal Waypoints Canceled")
        self.agent.waypoints[vehicle_position] = False

    # ROS1
    def velocity_publisher(self, vel, vehicle_position):
        frequency = 50
        rate = rospy.Rate(frequency)
        self.agent.log_info(f"[ROSHandler] Stopping vehicle: {self.agent.vehicle_names[vehicle_position]}")
        while self.slow_down[vehicle_position]:
            self.agent.cmd_vel_publisher[vehicle_position].publish(vel)
            rate.sleep()

    def slow_down_handler(self, _, data: dict, vehicle: str):
        """Handles slowdown messages and sends to specific vehicle parameters"""

        if self.agent.node.traffic_control:
            max_vel_x = data["data"]["max_vel_x"]
            max_vel_theta = data["data"]["max_vel_theta"]
            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            self.agent.log_info(
                f"[ROSHandler] Collision situation detected for vehicle: {self.agent.vehicle_names[vehicle_position]} ")

            # ROS 1
            vel = parse_vel(max_vel_x)
            x = threading.Thread(target=self.velocity_publisher, args=(vel, vehicle_position), daemon=True)
            threads = []
            for index in range(self.agent.number_of_vehicles):
                threads.append(x)
            if not self.slow_down[vehicle_position]:
                self.slow_down[vehicle_position] = True
                threads[vehicle_position].start()

    def collision_clearance_handler(self, _, data: dict, vehicle: str):
        """Handles clear collision messages and sends to specific vehicle parameters"""
        vehicle_position = self.check_fleet_get_vehicle_position(vehicle)

        if data["data"]["message_type"] == "slow_down":
            self.agent.log_info(f"[ROSHandler] Restarting vehicle: {self.agent.vehicle_names[vehicle_position]}")

            # ROS1
            self.slow_down[vehicle_position] = False

        # elif data["data"]["message_type"] == "path_rerouting":
        #    path_data = {"path": None, "rotation_angles": None}
        #    vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
        #    self.agent.log_info(
        #        f"[ROSHandler] Path Rerouting Clearance for vehicle: {self.agent.vehicle_names[vehicle_position]}")
        #    path_data["rotation_angles"] = [data["data"]["task_data"]["location"]["rotation"]]
        #    metric = data["data"]["task_data"]["location"]["metric"]
        #    path_data["path"] = [[metric["x"], metric["y"]]]
        #    self.path_rerouting_handler(_, path_data, vehicle)

    def docking_routine_request_handler(self, data: dict, vehicle: str):
        self.agent.log_info("[ROSHandler] Docking Routine RECORDING request")
        try:
            vehicle_position = self.check_fleet_get_vehicle_position(vehicle)
            vehicle = self.agent.vehicles[vehicle_position]
            vehicle_prefix = str(vehicle["prefix"])

            if vehicle_prefix == "None":
                vehicle_prefix = ""

            # ROS1
            self.client[vehicle_position] = actionlib.SimpleActionClient("docking", meili_agent.msg.DockingAction)
            self.agent.log_info("[ROSHandler] Waiting for docking server")
            self.client[vehicle_position].wait_for_server()

            goal = DockingGoal()
            goal.vehicle_position = vehicle_position
            goal.frequency = 30
            string = String()
            string.data = vehicle_prefix
            goal.vehicle_prefix = string
            self.agent.log_info("Before sending goal")
            self.client[vehicle_position].send_goal(goal)

        except Exception as e:
            self.agent.log_error(f"[ROSHandler] Error on Docking Routine Request: {e}")

    def topic_handler(self, _, topics, vehicle):
        """find vehicle token to passed to the topic streaming node"""

        if topics and vehicle:

            # initialise custom msg to send topic info

            for index in topics["data"]:
                self.agent.total_topics += 1
                index["vehicle_uuid"] = vehicle

                msg = parse_topic(index, self.agent)

                self.agent.topic_streaming_talker(msg)
                self.agent.log_info(
                    f"[ROSHandler] Topic to stream {index['topic']}, for vehicle {vehicle}"
                )
        else:
            rospy.loginfo("[ROSHandler] No topics to stream")

    def end_recording_publisher(self, vehicle_prefix):
        pub = rospy.Publisher(
            vehicle_prefix + "/recording", Bool, queue_size=5
        )
        return pub
