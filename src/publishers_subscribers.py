import sys
from importlib import import_module

import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Path
from sentry_sdk import capture_exception
from rosgraph_msgs.msg import Log


def get_message_type(message_type):
    try:
        assert sys.version_info >= (2, 7)  # import_module's syntax needs 2.7
        connection_header = message_type.split("/")
        ros_pkg = connection_header[0] + ".msg"
        msg_type = connection_header[1]
        msg_class = getattr(import_module(ros_pkg), msg_type)
        return msg_class
    except Exception as error:
        rospy.logerr(f"[ROSAgent] Error when getting message type {message_type}: {error}")
        raise


class PubSub:

    def __init__(self, node, agent):
        self.node = node
        self.agent = agent

    def get_prefix(self, vehicle):
        vehicle_prefix = str(vehicle["prefix"])
        if vehicle_prefix == "None":
            vehicle_prefix = ""
            prefix_mod2 = ""
        else:
            prefix_mod2 = vehicle_prefix[1: len(vehicle_prefix)]
            if self.agent.number_of_vehicles >= 2:
                prefix_mod2 = "_" + prefix_mod2

        return vehicle_prefix, prefix_mod2

    def battery_sub(self, vehicle):
        """Initiating Battery Subscriber"""
        battery_msg = get_message_type(
            self.agent.vehicles[0]["topics"]["battery"]["messageType"]
        )
        rospy.Subscriber(
            vehicle["topics"]["battery"]["topic"],
            battery_msg,
            self.agent.callback_battery,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    def gps_sub(self, vehicle):
        gps_msg = get_message_type(self.agent.vehicles[0]["topics"]["gps"]["messageType"]
                                   )
        rospy.Subscriber(
            vehicle["topics"]["gps"]["topic"],
            gps_msg,
            self.agent.callback_gps,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    def pose_sub(self, vehicle):
        pose_msg = get_message_type(self.agent.vehicles[0]["topics"]["pose"]["messageType"])

        rospy.Subscriber(
            vehicle["topics"]["pose"]["topic"],
            pose_msg,
            self.agent.callback_pose,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    def status_sub(self, vehicle):
        goalArray_msg = get_message_type(
            self.agent.vehicles[0]["topics"]["goalArray"]["messageType"]
        )
        rospy.Subscriber(
            vehicle["topics"]["goalArray"]["topic"],
            goalArray_msg,
            self.agent.callback_status_ros,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    def goal_pub(self, vehicle):
        goal_msg = get_message_type(self.agent.vehicles[0]["topics"]["goal"]["messageType"])

        self.agent.goal_publisher.append(
            rospy.Publisher(
                vehicle["topics"]["goal"]["topic"], goal_msg, queue_size=5
            )
        )

    def goal_cancel_pub(self, vehicle):
        goalCancel_msg = get_message_type(
            self.agent.vehicles[0]["topics"]["goal_cancel"]["messageType"]
        )
        self.agent.goal_canceller.append(
            rospy.Publisher(
                vehicle["topics"]["goal_cancel"]["topic"],
                goalCancel_msg,
                queue_size=5,
            )
        )

    def rosout_sub(self):
        rospy.Subscriber(
            'rosout_agg',
            Log,
            self.agent.callback_rosoutagg_ros,
            tcp_nodelay=True
        )

    def plan_sub(self, vehicle):
        vehicle_prefix, prefix_mod2 = self.get_prefix(vehicle)
        rospy.Subscriber(
            vehicle_prefix + "/move_base" + prefix_mod2 + "/NavfnROS/plan",
            Path,
            self.agent.callback_plan,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    def speed_sub(self, vehicle):
        speed_msg = get_message_type(
            self.agent.vehicles[0]["topics"]["speed"]["messageType"])

        rospy.Subscriber(
            vehicle["topics"]["speed"]["topic"],
            speed_msg,
            self.agent.callback_speed,
            vehicle["uuid"],
            tcp_nodelay=True,
        )

    # ROS1
    def vel_pub(self, vehicle):
        vehicle_prefix, prefix_mod2 = self.get_prefix(vehicle)
        self.agent.cmd_vel_publisher.append(rospy.Publisher(vehicle_prefix + "/cmd_vel", Twist, queue_size=5))

    # ROS1
    def traffic_control_setup(self, vehicle, index):
        vehicle_prefix, prefix_mod2 = self.get_prefix(vehicle)

        self.agent.max_vel_x[index] = rospy.get_param(
            vehicle_prefix
            + "/move_base"
            + prefix_mod2
            + "/DWAPlannerROS"
            + "/max_vel_x"
        )
        self.agent.max_vel_theta[index] = rospy.get_param(
            vehicle_prefix
            + "/move_base"
            + prefix_mod2
            + "/DWAPlannerROS"
            + "/min_vel_theta"
        )


def pub_and_sub(node, agent):
    """Creating Subscriber and Publisher for ROS1 Nodes"""
    pubsub = PubSub(node, agent)
    node.log_info("[PubSub] Starting Subscription and Publishing of Topics")

    try:
        for index, vehicle in enumerate(agent.vehicles):

            pubsub.pose_sub(vehicle)
            pubsub.status_sub(vehicle)
            pubsub.rosout_sub()

            pubsub.goal_pub(vehicle)
            pubsub.goal_cancel_pub(vehicle)

            pubsub.speed_sub(vehicle)
            # ROS1
            pubsub.vel_pub(vehicle)

            if node.battery_present:
                pubsub.battery_sub(vehicle)

            if node.outdoor:
                pubsub.gps_sub(vehicle)

            if node.traffic_control:
                pubsub.plan_sub(vehicle)
                # ROS1
                pubsub.traffic_control_setup(vehicle, index)
                node.log_info(f"[PubSub] Traffic Control Activated for {agent.vehicle_names[index]}")

            name = vehicle["prefix"]
            node.log_info(f"[PubSub] Completed publishing and subscription of topics for vehicle: {name}")

    except Exception as error:
        capture_exception(error)
        rospy.logerr(
            f"[PubSub] Publishing and subscription error: {error}"
        )
