#!/usr/bin/env python3
# standard python3 libraries
""" Agent Management"""
import datetime
import sys
from os.path import dirname, abspath, join

import numpy

# ROS1 dependencies
import rospy
from meili_agent.msg import Topicinit

# Meili agent dependencies
from meili_ros_lib.sentry import initialize_sentry, agent_sentry
from meili_ros_lib.agent import Agent

initialize_sentry()


class MeiliAgent(Agent):
    """
    Class for interact with ROS and Meili Cloud.
    """

    def __init__(self, node):
        if len(sys.argv) < 1:
            rospy.loginfo("[Config]usage: agent.py pathToConfigTopics")
            sys.exit()
        else:
            config_topics_path = sys.argv[1]
            config_vehicles_path = sys.argv[2]

        dir_topics = dirname(__file__)
        dir_config = dirname(dirname(abspath(__file__)))

        file_name_topics = abspath(join(dir_topics, config_topics_path))
        file_name_vehicles = abspath(join(dir_config, config_vehicles_path))

        Agent.__init__(self, node, file_name_vehicles, file_name_topics)

        # ROS1
        self.cmd_vel_publisher = []

        # ROS1 Goal
        self.goal_publisher = []
        self.goal_canceller = []


    def callback_rosoutagg_ros(self, msg):
        bytes = [1, 2, 4, 8, 16]  # ROS1
        levels = numpy.array(['debug', 'info', 'warning', 'error', 'fatal'])
        type_msg = levels[bytes.index(msg.level)]

        time_stamp = str(datetime.datetime.fromtimestamp(msg.header.stamp.secs))  # ROS1
        time_stamp.replace(":", "-")

        self.callback_rosoutagg(msg, type_msg, time_stamp)

    def callback_status_ros(self, msg, vehicle_uuid):
        # ROS1
        if msg.status_list and len(msg.status_list) != 0:
            last_status = msg.status_list[len(msg.status_list) - 1]
            goal_id = last_status.goal_id.id
            status_id = last_status.status

            vehicle_position = self.return_vehicle_position(vehicle_uuid)

            self.callback_status(msg, goal_id, status_id, vehicle_uuid)

    ################################################


# ROS1
def topic_streaming_talker(msg):
    """Publish Ros Message to topic streaming node if there is topics to stream"""
    pub = rospy.Publisher("/topic_init", Topicinit, queue_size=10)
    rate = rospy.Rate(1)  # 10hz
    # wait for having the topic streaming node subscribe
    while pub.get_num_connections() < 1:
        pass
    pub.publish(msg)
    rate.sleep()

################################################
